<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Produits;


class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit')]
    public function index(
        ManagerRegistry $doctrine,
        
        ): Response
    {

        $products = $doctrine->getRepository(Produits::class)->findAll();
        

        return $this->render('produit/index.html.twig', [
            
            'produits' => $products
        ]);
    }





}
